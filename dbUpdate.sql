CREATE TABLE IF NOT EXISTS user_requests(
   id serial PRIMARY KEY,
   user_id integer,
	request_message varchar(400),
   is_processed boolean  default false,

   CONSTRAINT user_requests_user_id_fkey FOREIGN KEY (user_id)
        REFERENCES users(id) MATCH SIMPLE
        ON UPDATE NO ACTION ON DELETE CASCADE,

   createdAt timestamp NOT NULL DEFAULT NOW(),
   updatedAt timestamp
);