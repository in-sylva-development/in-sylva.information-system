#!/bin/sh
# Expects one argument: the output directory for the generated certificate files

OUTPUT_DIR=${1:-"/tmp/certs"}

# Create the output directory if it doesn't exist
mkdir -p ${OUTPUT_DIR}
cd ${OUTPUT_DIR}

# Variables
COUNTRY="FR"
STATE="PACA"
LOCALITY="Avignon"
ORGANIZATION="INRAE"
ORG_UNIT="URFM"
COMMON_NAME="URFM"
EMAIL="philippe.clastre@inrae.fr"

# Generate a private key
openssl genrsa -out fullchain.pem.key 2048

# Create a certificate signing request (CSR) with provided subject information
openssl req -new -key fullchain.pem.key -out cert.csr -subj "/C=${COUNTRY}/ST=${STATE}/L=${LOCALITY}/O=${ORGANIZATION}/OU=${ORG_UNIT}/CN=${COMMON_NAME}/emailAddress=${EMAIL}"

# Generate a self-signed certificate
openssl x509 -req -days 365 -in cert.csr -signkey fullchain.pem.key -out fullchain.pem

# Clean up the CSR
rm cert.csr

echo "Self-signed certificate generated:"
echo "  Private key: fullchain.pem.key"
echo "  Certificate: fullchain.pem"
