#!/bin/bash
#
# SSL certificate autorenewer with certbot
# script installed in root crontab at deploy
cd $1
script="docker compose stop reverse-proxy; sleep 10; docker compose -f docker-compose.certs.yml up -d install-certs; sleep 10; docker compose -f docker-compose.certs.yml down -v; docker compose up -d reverse-proxy"
# Delete old script
CRON_SCHEDULE="0 2 * * *"
crontab -l | grep -v "$script" | crontab -
# add the new script
echo "Add to crontab : "
echo "$CRON_SCHEDULE  $script "
(
   crontab -l
   echo "$CRON_SCHEDULE $script "
) | crontab -u $USER -
echo "=> certificate renewer UPDATED in crontab for User : $USER "
