#!/bin/bash
#
# script pour dumper les données sur mongodb de la 20.44
#

DATADIR=dump_mongo
USER=$(cat $ENV | grep MONGO_INITDB_ROOT_USERNAME | awk -F'=' '{print $2}' | sed -e 's/ //g')
PWD=$(cat $ENV | grep MONGO_INITDB_ROOT_PASSWORD | awk -F'=' '{print $2}'| sed -e 's/ //g')

if [ -d $DATADIR ]; then
  rm -rf $DATADIR
  echo "INFO: suppression du dossier $DATADIR existant"
fi

mongodump --host="localhost:27017" -u=$USER -p=$PWD -o=$DATADIR
if [ $? -ne 0 ]; then
	exit -6
fi

# suppression de la partie admin qui pourrait poser problème à la restauration
rm -rf ${DATADIR}/admin

if [ -d $DATADIR ]; then
  echo "INFO: dump mongodb OK dans $DATADIR"
  exit 0
else
  echo "ERREUR génération du dump mongodb $DATADIR"
  exit -7
fi


