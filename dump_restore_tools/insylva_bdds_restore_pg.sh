#!/bin/bash
#
# script de sauvegarde des bases nécessaires pour le fonctionnement du SI insylva
# dans le but d'une restauration en cas de changements mineurs
#

export ENV=$1
export PGPASSWORD=$(cat $ENV | grep POSTGRES_PASSWORD | awk -F'=' '{print $2}'| sed 's/\r//g' )

PGUSER=$(cat $ENV | grep POSTGRES_USER | awk -F'=' '{print $2}')
STRCON="-h $SERVEUR_IP -p 5432 -U $PGUSER"
DATE=$(date '+%Y_%m_%d_%s')
howmany() { echo $#; }
DUMPPGDIR=dump_pg

files=$(find ./$DUMPPGDIR -name "dump_insylva_insylva*" -print)
if [ $(howmany $files) -lt 1 ]; then
  echo "ERROR: unable to find any insylva database dump. You should check your archive."
  exit
fi

if [ $(howmany $files) -gt 1 ]; then
  echo "ERROR: there are more than one in-sylva database dump file. Clean dump directory and restart."
  exit
else
  DUMP_INSYLVA=$files
fi

files=$(find ./$DUMPPGDIR -name "dump_insylva_keycloak*" -print)
if [ $(howmany $files) -lt 1 ]; then
  echo "ERREUR: unable to find any keycloak database dump. You should check your archive."
  exit
fi

if [ $(howmany $files) -gt 1 ]; then
  echo "ERROR: there are more than one keycloak database dump file. Clean dump directory and restart."
  exit
else
  DUMP_KEYCLOAK=$files
fi

echo "INFO: stoping containers that may interact with database server"
docker container stop in-sylva.gatekeeper in-sylva.source.manager in-sylva.keycloak in-sylva.pgadmin

docker exec in-sylva.postgres psql $STRCON -U $PGUSER -c "drop database insylva;"
docker exec in-sylva.postgres psql $STRCON -U $PGUSER -c "create database insylva;"
docker exec in-sylva.postgres psql $STRCON -U $PGUSER -c "drop database keycloak;"
docker exec in-sylva.postgres psql $STRCON -U $PGUSER -c "create database keycloak;"


docker cp $DUMP_INSYLVA in-sylva.postgres:/tmp/dump_insylva
docker cp $DUMP_KEYCLOAK in-sylva.postgres:/tmp/dump_keycloak

echo "INFO: restoring insylva database with dump file $DUMP_INSYLVA"
docker exec in-sylva.postgres pg_restore $STRCON --no-owner --role=$PGUSER -U $PGUSER --disable-triggers -d insylva /tmp/dump_insylva
echo "INFO: restoring keycloak database with dump file $DUMP_KEYCLOAK"
docker exec in-sylva.postgres pg_restore $STRCON --no-owner --role=$PGUSER -U $PGUSER --disable-triggers -d keycloak /tmp/dump_keycloak

# Cannot find this file dbUpdata.sql. These lines should be deleted in the future
#echo "INFO: running dbUpdate.sql"
#psql $STRCON -c "\i ../dbUpdate.sql" insylva
#echo "INFO: dbUpdate.sql Done"


