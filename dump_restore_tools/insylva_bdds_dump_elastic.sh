#!/bin/bash
# script de sauvegarde des données elasticsearch de la 20.44
#


USER=$(cat $ENV | grep ELASTICSEARCH_USERNAME | awk -F'=' '{print $2}')
PASSWORD=$(cat $ENV | grep ELASTICSEARCH_PASSWORD | awk -F'=' '{print $2}')
export PGPASSWORD=$(cat $ENV | grep POSTGRES_PASSWORD | awk -F'=' '{print $2}')
PGUSER=$(cat $ENV | grep POSTGRES_USER | awk -F'=' '{print $2}')
DUMPDIR=dump_elastic
DUMPFILE=elastic.json

if [ -d $DUMPDIR ]; then
  rm -rf $DUMPDIR
  echo "INFO: suppression dossier $DUMPDIR existant"
fi
mkdir $DUMPDIR

LISTE_INDEX=$(psql -qAtx -h localhost -U $PGUSER -c "select index_id from public.sources_indices where index_id is not null;" insylva| sed -e 's/index_id|//') 
echo "Liste des index à dumper: $LISTE_INDEX"
docker run --net=host -i -v $(pwd)/$DUMPDIR:/data taskrabbit/elasticsearch-dump \
--direction=dump \
--match="$LISTE_INDEX" \
--input=http://${USER}:${PASSWORD}@${SERVEUR_IP}:9200  \
--output=/data/$DUMPFILE \
--parallel=2 

if [ $? -ne 0 ]; then
	exit -8
fi

if [ -e ${DUMPDIR}/$DUMPFILE ]; then
  echo "INFO: dump elasticsearc OK dans ${DUMPDIR}/$DUMPFILE"
  exit 0
else
  echo "ERREUR génération du dump elastic ${DUMPDIR}/$DUMPFILE"
  exit -9
fi
 
