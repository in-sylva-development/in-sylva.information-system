#!/bin/bash
#
# script pour dumper les données sur mongodb de la 20.44
#

export ENV=$1

DATADIR=dump_mongo
USER=$(cat $ENV | grep MONGO_INITDB_ROOT_USERNAME | awk -F'=' '{print $2}' | sed -e 's/ //g')
PWD=$(cat $ENV | grep MONGO_INITDB_ROOT_PASSWORD | awk -F'=' '{print $2}'| sed -e 's/ //g')


echo "INFO: restore mondodb database in progress ..."

# copy dump file in the container
docker cp $DATADIR in-sylva.mongodb:/tmp/dump_mongo

# restore dump in an empty mongo database
docker exec in-sylva.mongodb mongorestore --drop -h mongo --port 27017 -u $USER -p $PWD /tmp/dump_mongo

 
echo "INFO: RESTORE mongodb OK"


