#!/bin/bash
#
# script used to restore all in-sylva information system data from an archive generated with insylva_dump_all.sh
#

if [ $# -lt 1 ]; then
   echo "Usage: $0 <archive.tar>"
   echo "<archive.tar>: tar archive used for the restore"
   exit
else
  ARCHIVE=$1
fi

if [ ! -e $ARCHIVE ]; then
  echo "ERROR: cannot find the tar file $ARCHIVE"
  exit
fi

export ENV=../.env
export SERVEUR_IP=$(grep SERVEUR_PRODUCTION_IP_ADDRESS $ENV | awk -F'=' '{print $2}')

  echo "       ______________________________________"
  echo "                    WARNING"
  echo " You're going to replace all data in the current instance of In-Sylva Information System."
  echo " Don't do this unless you know what you are doing."
  echo " This tool may be used in several cases:"
  echo " * Restore the infrastructure after an unexpected shutdown"
  echo " * Restore data in another instance of the In-Sylva IS"
  echo ""
  echo " Reminder: this tool will restore every data source, user account, role and policy from the dump."
  echo " At the end of this process, this In-Sylva instance will be an exact copy of the one that has generated the archive."
  echo "       ______________________________________"
  echo "  Press Enter to confirm or Ctrl+C to abort"
  read rep
  echo "       ______________________________________"
  echo "  Are you sure? Last chance..."
  echo "  Press Enter to confirm or Ctrl+C to abort"
  read rep

# delete old dump files
rm -rf dump_pg dump_mongo dumpo_elastic
# extract dump files from the archive
tar xvf $ARCHIVE

# use each script to restore data
./insylva_bdds_restore_mongo.sh $ENV
./insylva_bdds_restore_elastic.sh $ENV
./insylva_bdds_restore_pg.sh $ENV

echo "======================================================="
echo -n "Press Enter to restart all containers"
read rep
cd ..
export ENV=.env
echo "INFO: restarting all containers"
docker compose restart

USER=$(grep ELASTICSEARCH_USERNAME $ENV| awk -F"=" '{print $2}' | tr -d '\r\n')
PASSWORD=$(grep ELASTICSEARCH_PASSWORD $ENV| awk -F"=" '{print $2}' | tr -d '\r\n')

echo "INFO: Launching this command every 5 seconds:"
echo  "     curl -s -XGET \"http://$USER:$PASSWORD@localhost:9200/_cluster/health?pretty=true"

echo -n "INFO: Waiting for elasticsearch container to restart ...5 "
i=5
while :
do
  sleep 5
  curl -s -XGET "http://$USER:$PASSWORD@localhost:9200/_cluster/health?pretty=true" | grep cluster_name >/dev/null 2>&1
  if [ $? -eq 0 ]; then
    echo ""
    break
  fi
  i=$(expr $i + 5)
  echo -n "$i "
done
echo "INFO: elasticsearch OK"

docker container restart in-sylva.search.api

echo "INFO: all data should be restored"
