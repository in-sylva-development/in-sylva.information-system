#!/bin/bash
# script de restauration des données elasticsearch du projet INSYLVA
#

export ENV=$1

USER=$(cat $ENV | grep ELASTICSEARCH_USERNAME | awk -F'=' '{print $2}' |tr -d '\r\n')
PASSWORD=$(cat $ENV | grep ELASTICSEARCH_PASSWORD | awk -F'=' '{print $2}'|tr -d '\r\n')
DUMPDIR=dump_elastic
DUMPFILE=elastic.json

docker run --net=host --rm -ti -v $(pwd)/$DUMPDIR:/data taskrabbit/elasticsearch-dump \
--direction=load \
--output=http://${USER}:${PASSWORD}@localhost:9200  \
--input=/data/$DUMPFILE \
--parallel=2

echo "INFO: elasticsearch data restore OK"
