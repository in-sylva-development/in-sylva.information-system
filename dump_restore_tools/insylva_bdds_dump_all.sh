#!/bin/bash
#
# script d'enchainement des dumps (postgres, mongodb, elastic) pour le projet IN-sylva
#

export ENV=../.env
export SERVEUR_IP=$(grep SERVEUR_PRODUCTION_IP_ADDRESS $ENV | awk -F'=' '{print $2}')

./insylva_bdds_dump_pg.sh
if [ $? -ne 0 ]; then
	echo "Erreur: la sauvegarde de postgre n'a pas pu se faire."
	echo "Pas de sauvegarde réalisée"
	exit
fi
./insylva_bdds_dump_mongo.sh
if [ $? -ne 0 ]; then
	echo "Erreur: la sauvegarde de mongo n'a pas pu se faire."
	echo "Pas de sauvegarde réalisée"
	exit
fi

./insylva_bdds_dump_elastic.sh
if [ $? -ne 0 ]; then
	echo "Erreur: la sauvegarde d'elastic n'a pas pu se faire."
	echo "Pas de sauvegarde réalisée"
	exit
fi

PG=$(cat ./insylva_bdds_dump_pg.sh | grep DUMPPGDIR= | awk -F'=' '{print $2}')
MONGO=$(cat ./insylva_bdds_dump_mongo.sh | grep DATADIR= | awk -F'=' '{print $2}')
ELASTIC=$(cat ./insylva_bdds_dump_elastic.sh | grep DUMPDIR= | awk -F'=' '{print $2}')

# principe de fonctionnement des sauvegardes
# on conserve toutes les sauvegardes journalières  du lundi au vendredi (déclenchée par cron)
# si on est au début d'un mois (< 7), on conserve le premier lundi du mois en cours et on renomme le fichier dump_archive_mois.tar
# au total on devrait avoir  19 fichier dump_archive maximum
jour_du_mois=$(date +%d)

if [ $jour_du_mois -eq 1 ]; then    
        # si on est le premier du mois, alors on stocke l'archive pour le mois
	tar cvf DUMPS/dump_archive_$(date +%B)_$(date +%Y).tar $PG $MONGO $ELASTIC
else
	# sinon, c'est une archive par jour, remplacées la semaine suivante
	tar cvf DUMPS/dump_archive_$(date +%A).tar $PG $MONGO $ELASTIC
fi


# Contenu du crontab  pour déclencher les sauvegardes

# pour l'archive insylva mensuelle (conservée)
# 0 0 1 * * bash -c 'cd /usr/local/insylva/in-sylva.information-system/dump_restore_tools && ./insylva_bdds_dump_all.sh'
# pour les archives insylva hebdomadaires (générées le vendredi, remplacée chaque semaine)
# 10 0 * * 5 bash -c 'cd /usr/local/insylva/in-sylva.information-system/dump_restore_tools && ./insylva_bdds_dump_all.sh'
# pour les archives insylva quotidiennes (générées les lundi mardi, mercredi et jeudi, remplacée chaque semaine)
# 30 0 * * 1-4 bash -c 'cd /usr/local/insylva/in-sylva.information-system/dump_restore_tools && ./insylva_bdds_dump_all.sh'
# pour le transfert vers le s3
# 30 1 * * 1-5 bash -c 'cd /usr/local/insylva/in-sylva.information-system/dump_restore_tools && ./send_dumps_to_s3.sh'


