#
# script pour déposer les dumps sur un espace S3
#

DIR=$(pwd)

# récupération des dumps depuis un bucket S3 
S3CONFIG=$DIR/s3config_file

echo "INFO: synchronisation des dumps sur ressource S3 "
s3cmd -v -c $S3CONFIG put --force --check-md5 DUMPS/*.tar s3://0629-archive-insylva/DUMPS/

