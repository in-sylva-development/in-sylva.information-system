#!/bin/bash
#
# script de sauvegarde des bases nécessaires pour le fonctionnement du SI insylva
# dans le but d'une restauration en cas de changements mineurs
#
export PGPASSWORD=$(cat $ENV | grep POSTGRES_PASSWORD | awk -F'=' '{print $2}')
PGUSER=$(cat $ENV | grep POSTGRES_USER | awk -F'=' '{print $2}')
STRCON="-h $SERVEUR_IP -p 5432 -U $PGUSER"
DATE=$(date '+%Y_%m_%d_%s')
DUMPPGDIR=dump_pg
DUMP1=${DUMPPGDIR}/dump_insylva_insylva_$DATE
DUMP2=${DUMPPGDIR}/dump_insylva_keycloak_$DATE

pg_isready $STRCON > /dev/null 2>&1
if [ $? -ne 0 ]; then
	exit -2
fi

if [ -d $DUMPPGDIR ]; then
   echo "INFO: suppression dossier $DUMPPGDIR existant"
   rm -rf $DUMPPGDIR
fi

mkdir $DUMPPGDIR

# Options:
# -c: rajoute les instructions de nettoyage pour la restauration
# -F c: format pour pgrestore

pg_dump $STRCON  -f $DUMP1 -F c insylva
if [ $? -ne 0 ]; then
	exit -3
fi
pg_dump $STRCON  -f $DUMP2  -F c keycloak
if [ $? -ne 0 ]; then
	exit -4
fi

if [ -e $DUMP1 -a -e $DUMP2 ]; then
  echo "INFO: dumps générés dans $DUMPPGDIR"
  exit 0
else
  echo "ERREUR: problème de génération d'au moins un fichier parmi $DUMP1, $DUMP2"
  exit -5
fi

