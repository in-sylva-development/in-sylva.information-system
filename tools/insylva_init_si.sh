#!/bin/bash
#
# script d'initialisation du SI insylva 
#  Guide l'utilisateur pour les actions à faire après le build.sh
#

docker compose up -d

echo "Patientez 1mn ..."
sleep 60


echo "=======================================================" 
echo "Procédure insylva pour activation SI après build.sh"
echo -n "Presser  enter pour poursuivre  " 
read rep 
USERPGADMIN=$(cat .env | grep PGADMIN_DEFAULT_EMAIL | awk -F'=' '{print $2}')
PASSWORDPGADMIN=$(cat .env | grep PGADMIN_DEFAULT_PASSWORD | awk -F'=' '{print $2}')
USERPG=$(cat .env | grep POSTGRES_USER | awk -F'=' '{print $2}')
PASSWORDPG=$(cat .env | grep POSTGRES_PASSWORD | awk -F'=' '{print $2}')
KEYCLOAK_CONTAINER_NAME=$(docker ps --format "{{.Names}}" | grep keycloak )
USERKEYCLOAK=$(cat .env | grep KEYCLOAK_USER | awk -F'=' '{print $2}')
PASSWORDKEYCLOAK=$(cat .env | grep KEYCLOAK_PASSWORD | awk -F'=' '{print $2}')
SEARCH_CONTAINER_NAME=$(docker ps --format "{{.Names}}" | grep searchAPI )
SERVERIP=$(hostname -I | awk '{print $1}')

echo "       ______________________________________           "
echo "                   INFO: Etape 1"
echo "       ______________________________________           "

echo "             Mise en place du  REALM insylva"

# contrôle non saturation /var
dispo_sur_var=$(df -h | grep "/var" |awk '{print $4}' | sed -e 's/.[0-9]//' -e 's/[A-Z]//')
if [ $dispo_sur_var -lt 1 ]; then
	echo "WARNING: risque de saturation de l'espace sur /var ..."
	echo "Lancer la commande dockersystem prune si nécessaire"
fi



# Controle disponibilité pg
export PGPASSWORD=$PASSWORDPG
pg_isready -h 147.100.20.44 -p 5432 -U $USERPG  > /dev/null 2>&1
if [ $? -ne 0 ]; then
	echo "ERREUR: le serveur postgre ne semble pas disponible"
        exit
fi

psql -h localhost -U insylva_admin_pg keycloak -c "update REALM set ssl_required = 'NONE' where id = 'master';"
docker container restart $KEYCLOAK_CONTAINER_NAME 

echo "Patientez 30 secondes ..."
sleep 30

echo "       ______________________________________           "
echo "                    INFO: Etape 2"
echo "       ______________________________________           "
echo "      Se connecter à keycloak: "
echo "      http://$SERVERIP:7000/keycloak/auth/   crédential: user=$USERKEYCLOAK, password=$PASSWORDKEYCLOAK"
echo "      Puis, click sur Master, et choisir le menu Add Realm"
echo "      Importer le fichier realm-export.json (dans le dossier keycloak)"
echo -n "Presser enter quand c'est fait "
read rep
echo "Patientez 30 secondes ..."
sleep 30
echo "       ______________________________________           "
echo "                    INFO Etape 3"
echo "       ______________________________________           "
echo "      Création compte admin"
curl --location --request POST "http://$SERVERIP:4000/user/create-system-user"
if [ $? -ne 0 ]; then
	echo "Erreur: pb insertion compte admin dans la base"
	echo "Vérifiez gatekeeper, et relancez la commande: curl --location --request POST 'http://$SERVERIP:4000/user/create-system-user'"
	exit -1
fi

echo "      Relance de tous les containers "
docker compose restart search-api


