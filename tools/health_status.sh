#!/bin/bash

POSTGRES_CONTAINER=postgres
PG_USER=postgres
PG_PASSWORD=admin

# Keycloak controls
DB=keycloak
ret=$(docker compose exec -i $POSTGRES_CONTAINER psql -U $PG_USER -c "SELECT datname FROM pg_database WHERE datname = '$DB';"| grep "$DB" |sed 's/^[ \t]*//;s/[ \t]*$//')
if [[ "$ret" == "$DB" ]]; then
    echo "Keycloak database exists: On"
    DBOK=1
else
    echo -e "\e[91mKeycloak database exists: Off\e[0m"
    DBOK=0
fi

ret=$(docker compose exec -T $POSTGRES_CONTAINER psql -U $PG_USER -d $DB -c "\dt realm" | grep "realm")
if [[ $DBOK && $? -eq 0 ]]; then
  echo "Keycloak realm table exists: On"
  REALMOK=1
else
  echo -e "\e[91mKeycloak realm table exists: Off\e[0m"
  REALMOK=0
fi


ret=$(docker compose exec -i $POSTGRES_CONTAINER psql -U $PG_USER -d $DB -c "SELECT ssl_required FROM realm WHERE id = 'master';" | grep "1 row")
if [[ $REALMOK && "$ret" == "(1 row)" ]]; then
    echo "Keycloak master ssl configuration: On"
    SSLOK=1
else
    echo -e "\e[91mKeycloak master ssl configuration: Off\e[0m"
    SSLOK=0
fi

ret=$(docker compose exec -i $POSTGRES_CONTAINER psql -U $PG_USER -d $DB -c "SELECT * FROM realm WHERE id = 'in-sylva';" | grep "1 row")
if [[ $SSLOK && "$ret" == "(1 row)" ]]; then
    echo "Keycloak in-sylva realm is declared: On"
else
    echo -e "\e[91mKeycloak in-sylva realm is declared: Off\e[0m"
fi

# In-sylva controls
DB=insylva
ret=$(docker compose exec -i $POSTGRES_CONTAINER psql -U $PG_USER -c "SELECT datname FROM pg_database WHERE datname = '$DB';"| grep "$DB" |sed 's/^[ \t]*//;s/[ \t]*$//')
if [[ "$ret" == "$DB" ]]; then
    echo "In-Sylva database exists: On"
else
    echo -e "\e[91mIn-Sylva database exists: Off\e[0m"
fi

ret=$(docker compose exec -i $POSTGRES_CONTAINER psql -U $PG_USER -d $DB -c "SELECT * FROM users WHERE username = 'admin';" | grep "1 row")
if [[ "$ret" == "(1 row)" ]]; then
    echo "Admin in-sylva SI account created: On"
else
    echo -e "\e[91mAdmin in-sylva SI account created: Off\e[0m"
fi

# Elastic search is alive
export ENV=.env
USER=$(grep ELASTICSEARCH_USERNAME $ENV| awk -F"=" '{print $2}' | tr -d '\r\n')
PASSWORD=$(grep ELASTICSEARCH_PASSWORD $ENV| awk -F"=" '{print $2}' | tr -d '\r\n')

curl -s -XGET "http://$USER:$PASSWORD@localhost:9200/_cluster/health?pretty=true"| grep cluster_name >/dev/null 2>&1
if [ $? -eq 0 ]; then
    echo "ElasticSearch container is alive: On"
else
    echo -e "\e[91mElasticSearch container is alive: Off\e[0m"
fi