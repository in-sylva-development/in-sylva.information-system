#!/bin/bash
# Script to start In-sylva microservices containers
# Includes information for the user if it's the first time starting this project.

if [ ! -e .env ]; then
  echo "==========================================================="
  echo "  It seems it's the first time you execute this script."
  echo "  We have created a .env file that you MUST edit according to your needs."
  echo "  Current values are examples, thus not meaningful for the application yet."
  echo "  If you don't edit them, In-sylva IS will not work properly."
  echo "  For production purposes, you are to change ALL passwords to avoid huge security flaws."
  echo " "
  echo "  As a reminder, in this .env file, you will specify admin accounts credentials needed by in-sylva IS."
  echo "  You should take into account that this .env file will not be saved anywhere."
  echo " "
  echo "  Edit .env file and start this script again to start the project."
  echo "==========================================================="
  cp .env.template .env
  exit
fi

echo "INFO: Starting docker containers for IN-SYLVA IS. Nothing done if they're already running."
docker compose up -d
